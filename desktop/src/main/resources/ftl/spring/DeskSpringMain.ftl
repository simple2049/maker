package ${projectPackage};

import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import javafx.stage.Stage;
import ldh.fx.StageUtil;
import ${projectPackage}.controller.HomeView;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DeskSpringMain extends AbstractJavaFxApplicationSupport {

    @Override
    public void start(Stage stage) throws Exception {
        super.start(stage);
        StageUtil.STAGE = stage;
        stage.sceneProperty().addListener((b, o, scene)->{
            if (scene != null) {
                scene.getStylesheets().add("/css/common.css");
                scene.getStylesheets().add("/component/LNavPane.css");
                scene.getStylesheets().add("/component/LxDialog.css");
                scene.getStylesheets().add("/component/GridTable.css");
                scene.getStylesheets().add("bootstrapFx.css");
                scene.getStylesheets().add("/css/Form.css");
            }
        });
    }

    public static void main(String[] args) {
        launch(DeskSpringMain.class, HomeView.class, args);
    }
}