package ${projectPackage}.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import ldh.common.PageResult;
import ldh.fx.util.HttpClientFactory;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Map;

public class JavafxHttpUtil {

    public static <T> T get(String url, Class<T> clazz) throws Exception {
        String json = HttpClientFactory.getInstance().get(url);
        return ParseJson(clazz, json);
    }

    public static <T> T get(String url, TypeReference<T> type) throws Exception {
        String json = HttpClientFactory.getInstance().get(url);
        return ParseJson(type, json);
    }

    private static <T> T ParseJson(TypeReference<T> type, String json) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonObject = mapper.readTree(json);
        boolean isSuccess = jsonObject.get("isSuccess").asBoolean();
        if(isSuccess) {
            mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            T t = mapper.readValue(jsonObject.get("data").toString(), type);
            return t;
        } else {
            throw new RuntimeException("获取数据失败, data:" + json);
        }
    }

    private static <T> T ParseJson(Class<T> type, String json) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonObject = mapper.readTree(json);
        boolean isSuccess = jsonObject.get("isSuccess").asBoolean();
        if(isSuccess) {
            mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            T t = mapper.readValue(jsonObject.get("data").toString(), type);
            return t;
        } else {
            throw new RuntimeException("获取数据失败, data:" + json);
        }
    }

    public static <T> T get(String url, Map<String, Object> paramMap, Class<T> clazz) throws Exception {
        String json = HttpClientFactory.getInstance().get(buildUrl(url, paramMap));
        return ParseJson(clazz, json);
    }

    public static <T> PageResult<T> get(String url, Map<String, Object> paramMap, TypeReference type) throws Exception {
        String json = HttpClientFactory.getInstance().get(buildUrl(url, paramMap));
        return (PageResult)ParseJson(type, json);
    }

    public static <T> T post(String url, Map<String, Object> paramMap, Class<T> clazz) throws Exception {
        String json = HttpClientFactory.getInstance().post(url, paramMap);
        return ParseJson(clazz, json);
    }

    public static <T> T post(String url, Map<String, Object> paramMap, TypeReference<T> type) throws Exception {
        String json = HttpClientFactory.getInstance().post(url, paramMap);
        return ParseJson(type, json);
    }

    private static String buildUrl(String url, Map<String, Object> paramMap) {
        StringBuilder sb = new StringBuilder();
        boolean isFirst = true;

        Map.Entry entry;
        for(Iterator var4 = paramMap.entrySet().iterator(); var4.hasNext(); sb.append((String)entry.getKey()).append("=").append(entry.getValue().toString())) {
            entry = (Map.Entry)var4.next();
            if(isFirst) {
                isFirst = false;
            } else {
                sb.append("&");
            }
        }

        String u = url + "?" + sb.toString();
        System.out.println("url:" + u);
        return u;
    }
}
