<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<ul class="nav flex-column">
    <#list tableInfo.tables?keys as key>
    <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create>
	 <li class="nav-item">
        <a class="nav-link <c:if test="${r'${'}active == '${util.lowers(tableInfo.tables[key].javaName)}'${r'}'}">active</c:if>" href="${r'${pageContext.request.contextPath}'}/${util.firstLower(tableInfo.tables[key].javaName)}/list">
            <span data-feather="home"></span>
            ${util.comment(tableInfo.tables[key])}管理
        </a>
    </li>
	</#if>
	</#list>
</ul>