package ${package};

/**
 * @Auther: ${Author}
 * @Date: ${DATE}
<#if table.comment??>
 * @Description: ${table.comment}
</#if>
 */

<#if imports?exists>
	<#list imports as import>
import ${import};
	</#list>
</#if>

<#if serializable>@SuppressWarnings("serial")</#if>
<#if isLombok>
@Data
</#if>
public class ${className} <#if extend?exists>extends ${extend}</#if> <#if implements?exists>implements <#list implements as implement>${implement}<#if implement_has_next>,</#if></#list></#if>${r'{'}

	<#if table.columnList??>
	<#list table.columnList as column>
	<#if !column.foreign>
	<#if column.comment??>

	/**
	 * ${column.comment}
	 */
	</#if>
	private ${column.javaType} ${column.property};
	</#if>
	</#list>
	</#if>
	
	<#if table.foreignKeys??>
	<#list table.foreignKeys as foreignKey>
	<#if foreignKey.column.comment??>
	/**
	 * ${foreignKey.column.comment}
	 */
	</#if>
	private ${util.firstUpper(foreignKey.foreignTable.javaName)} ${foreignKey.column.property};
	</#list>
	</#if>
	<#-- 一对多中多的对象 -->
	<#if table.columnList??>
	<#list table.many as foreignKey>
	<#if foreignKey.table.create>
	<#if foreignKey.oneToOne>
	public ${util.firstUpper(foreignKey.table.javaName)} ${foreignKey.table.javaName};
	
	<#else>
	public List<${util.firstUpper(foreignKey.table.javaName)}> ${foreignKey.table.javaName}s;

	</#if>
	</#if>
	</#list>
	</#if>
	<#-- 多对多关系 -->
	<#if table.columnList?? && table.manyToManys??>
	<#list table.manyToManys as manyToMany>
	<#if util.isCreateMtm(table, manyToMany) && manyToMany.secondTable.create>
    //多对多关系
    public List<${util.firstUpper(manyToMany.secondTable.javaName)}> ${manyToMany.secondTable.javaName}s;
	
	</#if>
	</#list>
	</#if>
	<#-- ... -->
	<#if !isLombok>
	<#if table.columnList??>
	<#list table.columnList as column>
	<#if !column.foreign>
	public void set${util.upFirst(column.property)}(${column.javaType} ${column.property}) {
		this.${column.property} = ${column.property};
	}

	public ${column.javaType} get${util.upFirst(column.property)}() {
		return ${column.property};
	}
	
	</#if>
	</#list>
	</#if>
	<#-- 外键-->
	<#if table.foreignKeys??>
	<#list table.foreignKeys as foreignColumn>
	public void set${util.firstUpper(foreignColumn.column.property)}(${util.firstUpper(foreignColumn.foreignTable.javaName)} ${foreignColumn.column.property}) {
		this.${foreignColumn.column.property} = ${foreignColumn.column.property};
	}
	
	public ${util.firstUpper(foreignColumn.foreignTable.javaName)} get${util.firstUpper(foreignColumn.column.property)}() {
		return ${foreignColumn.column.property};
	}
	
	</#list>
	</#if>
	<#-- 一对多中多的对象 -->
	<#if table.columnList?? && table.many??>
	<#list table.many as foreignKey>
	<#if foreignKey.table.create>
	<#if !foreignKey.oneToOne>
	<#-- 外键设置-->
	<#if foreignKey??>
	public void set${util.firstUpper(foreignKey.table.javaName)}s(List<${util.firstUpper(foreignKey.table.javaName)}> ${foreignKey.table.javaName}s) {
		this.${foreignKey.table.javaName}s = ${foreignKey.table.javaName}s;
	}

	public List<${util.firstUpper(foreignKey.table.javaName)}> get${util.firstUpper(foreignKey.table.javaName)}s() {
		return this.${foreignKey.table.javaName}s;
	}

	</#if>
	</#if>
	</#if>	
	</#list>
	<#-- 多对多关系 -->
	<#if table.columnList?? && table.manyToManys??>
	<#list table.manyToManys as manyToMany>
	<#if util.isCreateMtm(table, manyToMany) && manyToMany.secondTable.create>
    //多对多关系
	public void set${util.firstUpper(manyToMany.secondTable.javaName)}s(List<${util.firstUpper(manyToMany.secondTable.javaName)}> ${manyToMany.secondTable.javaName}s) {
		this.${manyToMany.secondTable.javaName}s = ${manyToMany.secondTable.javaName}s;
	}
	
	public List<${util.firstUpper(manyToMany.secondTable.javaName)}> get${util.firstUpper(manyToMany.secondTable.javaName)}s() {
		return this.${manyToMany.secondTable.javaName}s;
	}
	
	</#if>
	</#list>
	</#if>
	</#if>
	</#if>
${r'}'}

