package ldh.maker.util;


import ldh.maker.database.TableInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by ldh on 2017/3/28.
 */
public class DbInfoFactory {

    private static DbInfoFactory instance = new DbInfoFactory();

    private Map<String, TableInfo> tableInfoMap = new ConcurrentHashMap<>();

    public static DbInfoFactory getInstance() {
        return instance;
    }

    public void put(String treeNodeId, TableInfo tableInfo) {
        tableInfoMap.put(treeNodeId, tableInfo);
    }

    public TableInfo get(String treeNodeId) {
        return tableInfoMap.get(treeNodeId);
    }
}