package ldh.maker.freemaker;

/**
 * Created by ldh on 2017/4/8.
 */
public class PomXmlMaker extends FreeMarkerMaker<PomXmlMaker> {

    protected String projectRootPackage;
    protected String projectName;

    public PomXmlMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }

    public PomXmlMaker projectName(String projectName) {
        this.projectName = projectName;
        return this;
    }

    @Override
    public PomXmlMaker make() {
        data();
        if (ftl == null) {
            this.out("pom.ftl", data);
        } else {
            this.out(ftl, data);
        }
        return this;
    }

    @Override
    public void data() {
        fileName = "pom.xml";
        data.put("projectRootPackage", projectRootPackage);
        data.put("projectName", projectName);
    }
}
