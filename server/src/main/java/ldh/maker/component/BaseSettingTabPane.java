package ldh.maker.component;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.*;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.db.TableViewDb;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.vo.TableViewData;
import ldh.maker.vo.TreeNode;
import org.controlsfx.control.MaskerPane;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ldh123 on 2018/5/5.
 */
public abstract class BaseSettingTabPane extends StackPane {

    protected TreeItem<TreeNode> treeItem;
    protected String dbName;
    protected MaskerPane maskerPane = new MaskerPane();
    private VBox container = new VBox();
    protected volatile boolean isShow = false;

    public BaseSettingTabPane(TreeItem<TreeNode> treeItem, String dbName) {
        this.treeItem = treeItem;
        this.dbName = dbName;

        container.setSpacing(5);
        container.setPadding(new Insets(5, 5, 5, 5));

        maskerPane.setText("正在加载中......");
        this.getChildren().addAll(container, maskerPane);
    }

    public void show() {
        if (isShow) return;
        new Thread(new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);
                while(!tableInfo.isLoadEnd() || !isLoadEnd()) {
                    Thread.sleep(100);
                }
                Platform.runLater(()->{
                    initUi();
                    maskerPane.setVisible(false);
                });
                isShow = true;
                return null;
            }
        }).start();
    }

    protected void initUi() {
        initHeader();
        Node node = initBody();
        VBox.setVgrow(node, Priority.ALWAYS);
        container.getChildren().add(node);
        initSubmitBtn();
    }

    protected abstract Node initBody();

    protected void initHeader() {
        HBox hbox = new HBox();
        Label label = new Label(getTitle());
        hbox.getChildren().add(label);
        hbox.getStyleClass().addAll("round-border", "title-background", "title");
        container.getChildren().add(hbox);
    }

    protected String getTitle() {
        return "请重构此方法";
    }

    protected void initSubmitBtn() {
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER_RIGHT);
        hbox.setSpacing(5);
        hbox.getStyleClass().add("round-border");

        Button clean = new Button("清空");
        clean.getStyleClass().addAll("btn", "btn-info");
        clean.setOnAction(this::clean);

        Button reset = new Button("重置");
        reset.getStyleClass().addAll("btn", "btn-info");
        reset.setOnAction(this::reset);

        Button submit = new Button("保存");
        submit.getStyleClass().addAll("btn", "btn-info");
        submit.setOnAction(this::save);

        hbox.getChildren().addAll(clean, reset,submit);
        hbox.getStyleClass().add("round-border");
        container.getChildren().add(hbox);
    }

    protected ColumnConstraints buildLabel(double width) {
        ColumnConstraints c1 = new ColumnConstraints(width);
        c1.setHalignment(HPos.RIGHT);
        return c1;
    }

    protected ColumnConstraints buildNode(double width) {
        ColumnConstraints c1 = new ColumnConstraints(width);
        c1.setHalignment(HPos.LEFT);
        return c1;
    }

    protected  void save(ActionEvent event) {}

    protected boolean check() {return false;}

    protected void reset(ActionEvent event) {}

    protected void clean(ActionEvent event) {}

    public boolean isLoadEnd() {
        return true;
    }
}
